function isMailValid(mail) {
  return /.+@.+.\w+/.test(mail);
}

function isPasswordValid(password) {
  return password.length > 3;
}

function sendRegistration() {
  const login = document.getElementById("login").value;
  const password = document.getElementById("password").value;
  const msgEl = document.getElementById("message");
  msgEl.innerText = "";

  if (!isMailValid(login)) {
    msgEl.innerText = "Your login should be a valid mail adress";
    return false;
  }

  if (!isPasswordValid(password)) {
    msgEl.innerText = "Your password should have at least 3 characters";
    return false;
  }

  const passwordHash = new Hashes.SHA256().b64(password);
  axios
    .post("http://localhost:3001/register", {
      login,
      password: passwordHash
    })
    .then(response => {
      msgEl.innerText = response.data;
    })
    .catch(error => {
      msgEl.innerText = error;
      console.error("Server error: ", error);
    });
}
