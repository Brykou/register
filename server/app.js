const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const nodemailer = require("nodemailer");
const low = require("lowdb");
const FileAsync = require("lowdb/adapters/FileAsync");
const path = require("path");
const config = require("./config");

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  cors({
    origin: "*",
    methods: ["GET", "POST", "DELETE"]
  })
);
const adapter = new FileAsync(config.database.name, {
  defaultValue: { users: [] }
});

low(adapter)
  .then(db => {
    app.post("/register", (req, res, next) => {
      const { login, password } = req.body;

      // Check if email is valid
      if (/.+@.+.\w+/.test(login) === null) {
        res.status(400).send("User login should be a valid email");
        return next();
      }

      // Check if user exist
      const userExist = db
        .get("users")
        .find({ login: login })
        .value();
      if (userExist) {
        res.status(400).send("This email already exist");
        return next();
      }

      db.get("users")
        .push({ login, password, isVerified: false })
        .write()
        .then(() => {
          const transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            secure: true,
            auth: {
              user: "zonamailbox@gmail.com",
              pass: "Bb123456!"
            }
          });
          const mailOptions = {
            from: "zonamailbox@gmail.com",
            to: login,
            subject: "Account validation",
            html: `click on this link to validate your registration : <a href="http://${
              config.host
            }:${config.port}/verify?user=${login}">Validate</a>`
          };

          transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
              res.status(500).send("Error sending confirmation mail");
              next();
            }
            res.status(200).send("Confirmation email has been sent");
          });
        })
        .catch(error => {
          res.status(500).send("Internal error");
        });
    });

    app.get("/verify", (req, res) => {
      const { user } = req.query;
      db.get("users")
        .find({ login: user })
        .assign({ isVerified: true })
        .write()
        .then(() => {
          res.status(200).sendFile(path.join(__dirname + "/verify.html"));
        })
        .catch(error => {
          res.status(500).send("Internal error");
        });
    });
  })
  .then(() => {
    app.listen(config.port, () =>
      console.log("Server is listening on port " + config.port)
    );
  });
